+++
title = "Clusterssh"
date = 2021-10-25T20:07:39+02:00
draft = false
tags = ["ssh","clusterssh","administration"]
categories = ["Allgemein"]
+++

Vor einiger Zeit stand ich vor der Herausforderung mehrere Notebooks zu administrieren. Konkret ging es um ein Upgrad von Debian 10 "Buster" zu Debian 11 "Bullseye". Ich suchte also nach einer vergleichsweisen einfachen und unkomplizierten Lösung. Schnell kam ich zu Tools wie [Puppet](https://puppet.com/open-source/#osp), [Ansible](https://www.ansible.com/) oder [Cockpit](https://cockpit-project.org/) und erkannte das alle Tools entweder viel zu komplex oder aber nicht den Funktionsumfang mitbrachten den ich suchte. Dank [nek0](https://nek0.eu/) lernte ich [clusterssh](https://sourceforge.net/projects/clusterssh/) kennen. Clusterssh erfüllte vollumfänglich meine Anforderungen die waren:
- möglichst einfaches Tool (konfiguration/installation)
- unter Debian lauffähig
- Verbindung via SSH

Installieren konnte ich Clusterssh aus den Standart Debian Repository via APT.
```shell=bash
   sudo apt install clusterssh
```
Daraufhin wird im Home Verzeichnis des Nutzers folgendes angelegt.
```shell=bash
   /home/USER
   ├── .clusterssh
   ├─── clusters
   └─── config
```
In der ```~/.clusterssh/config``` können verschiedene Parameter eingestellt werden, so unter anderem auch der jeweilige Terminal Emulator. Die eigentlich interessante und notwendige Datei ```~/.clusterssh/clusters``` muss hingegen händisch angelegt werden. Der Aufbau der Datei ist ebenso simple wie die vorherige Installation oder die spätere Benutzung.
  
```
   [Cluster-Name] [Maschine-1] [Maschine-2] [Maschine-3]
```

In meinem Szenario sah die folgendermaßen aus:
```
   wlan nac@wlan06 nac@wlan07 nac@wlan08 nac@wlan09 nac@wlan10 nac@wlan11 nac@wlan12 nac@wlan13
```

Fortan konnte ich nun also mit dem Befehl ```cssh wlan``` eine SSH Verbindung zu allen zugehörigen Maschinen (wlan06, wlan07, etc.) aufbauen. Das ganze sah dann wie folgt aus:
![ClusterSSH Bild](/img/cluster_ssh.png)
Das kleine Eingabefeld in der Mitte des Bildschirms übertrug alle Tastatureingaben zu allen verbundenen Clients. Somit konnte entsprechende Befehle zeitgleich auf allen Maschinen ausführen lassen. Gleichermaßen konnte ich aber auch jederzeit in eines der Terminalfenster springen um ggf. Client spezifische Dinge zu tun.  
  
Für mich hat sich Clusterssh damit bewährt. Installation und Konfiguration sind ebenso simple wie die Benutzung selbst. Für kleinere Netzwerke kann ich es nur wärmstens empfehlen.
